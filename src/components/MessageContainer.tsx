"use client"

import { useMessageStore } from '@/app/store';
import json5 from 'json5'

export default function MessageContainer() {
    const addMessage = useMessageStore((state: any) => state.addMessage);
    const messages = useMessageStore((state: any) => state.messages);

    // const intervalId = setInterval(async () => {
    //     const data = await get_messages('DEMO') as any;
    //     addMessage(JSON.parse(data?.message));
    // }, 10000);

    return (
        <div id="message-container" className="flex-grow overflow-y-auto pr-2" key='1'>
            <div className="flex-col items-start space-x-3 mt-4" key='2'>
                {messages.map((msg: any) => {
                    const _ = JSON.parse(msg);
                    console.log(_);
                    // const _msg = json5.parse(_.message);
                    // console.log(`${Object.entries(_msg)}  ---  ${_.message instanceof String}`);
                    // let result;
                    const result = _.from_emily ?
                        <div key={_.timestamp}>
                            <img
                                src="next.svg"
                                alt="Emily's Avatar"
                                className="avatar rounded-full flex-shrink-0"
                            />
                            <div className="flex flex-col w-full">
                                <p className="text-xs text-gray-400 sender-name">
                                    {_.from_emily ? 'Emily' : 'You'} <span className="timestamp">{Date.now()}</span>
                                </p>

                                {_.message.startsWith('{') ?
                                    <div className="bg-slate-300 p-2 rounded w-full mr-4"> <strong>Here's my step by step plan:</strong>
                                        <br />
                                        <br />
                                        {Object.entries(json5.parse(_.message)).map((ent: any[]) => <div key={ent[0]}><input type="checkbox" id={ent[0]} disabled ></input><label key="step-{k[0]}"><strong>Step {ent[0]}</strong>: {ent[1]}</label></div>)}
                                    </div>

                                    : /https?:\/\/[^\s]+/.test(_.message) ?
                                        <div className="bg-slate-800 p-2 rounded w-full mr-4">xxx</div>
                                        :
                                        <div className="msg-content  bg-slate-800 p-2 rounded w-full mr-4">{_.message}</div>
                                }
                            </div>
                        </div>
                        :
                        <div>
                            <img
                                src="next.svg"
                                alt="Emily's Avatar"
                                className="avatar rounded-full flex-shrink-0"
                            />
                            <div className="flex flex-col w-full">
                                <p className="text-xs text-gray-400 sender-name">
                                    {_.from_emily ? 'Emily' : 'You'} <span className="timestamp">{Date.now()}</span>
                                </p>
                                {/https?:\/\/[^\s]+/.test(_.message) ? <div className="bg-slate-800 p-2 rounded w-full mr-4">
                                    _.message.replace(/(https?:\/\/[^\s]+)/g, '<u><a href="$1" target="_blank">$1</a></u>')
                                </div> : <div className="msg-content bg-slate-800 p-2 rounded w-full mr-4" >{_.message}</div>
                                }
                            </div>
                        </div>

                    return result;

                })
                }


            </div>
        </div >
    );
}