export default function InternalMonologue() {
    return (
        <div
            className="internal-monologue bg-slate-950 border border-indigo-700 p-2 rounded mt-4 flex items-start space-x-3"
        >
            <img
                src="next.svg"
                alt="Emily's Avatar"
                className="avatar rounded-full flex-shrink-0"
            />
            <div>
                <p className="text-xs text-gray-400">Emily's Internal Monologue</p>
                <p className="text-sm">{'$agentState?.internal_monologue' || "😴"}</p>
            </div>
        </div>
    );
}