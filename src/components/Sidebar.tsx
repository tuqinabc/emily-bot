

export default function Sidebar() {
    function handleClick() {
        console.log('increment like count');
    }
    return (
        <div
            className="flex flex-col w-20 h-dvh fixed top-0 left-0 z-10 bg-slate-950 p-4 space-y-4 items-center shadow-2xl shadow-indigo-700"
        >
            <div className="p-5 rounded nav-button relative">
                <a href="/">
                    <button className="text-gray-400 hover:text-white flex justify-center w-full">
                        <i className="fas fa-home fa-lg"></i>
                    </button>
                </a>
                <span className="tooltip">Home</span>
            </div>
            <div className="p-5 rounded nav-button relative">
                <a href="/settings">
                    <button className="text-gray-400 hover:text-white flex justify-center w-full">
                        <i className="fas fa-cog fa-lg"></i>
                    </button>
                </a>
                <span className="tooltip">Settings</span>
            </div>
            <div className="p-5 rounded nav-button relative">
                <button className="text-gray-400 hover:text-white flex justify-center w-full">
                    <i className="fas fa-history fa-lg"></i>
                </button>
                <span className="tooltip">Logs</span>
            </div>
        </div>

    );
};

