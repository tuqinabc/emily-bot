"use client"
import { useMessageStore } from '@/app/store';
import { add_message_to_project, execute_agent, new_message } from '@/lib/lib';
import { planner_execute } from '@/lib/planner/planner';
import { useState } from 'react'

export default function MessageInput() {
    const messages = useMessageStore((state: any) => state.messages);
    const addMessage = useMessageStore((state: any) => state.addMessage);
    let isAgentActive = false;
    let messageInput = '';
    async function sendMessage() {
        console.log(messageInput);
        const projectName = localStorage.getItem('selectedProject');
        if (!projectName) {
            alert("please select a project first");
            return;
        }
        if (messageInput.trim() != "" && !isAgentActive) {
            console.log('execute messages');
            if (messages.length === 0) {
                console.log('no messages ---- ');
            } else {
                console.log('send messages ----  ');
            }
        }
        addMessage(JSON.stringify(new_message(false, messageInput)));
        const data = await add_message_to_project(projectName, JSON.stringify(new_message(false, messageInput)));
        const test = await execute_agent(projectName, messageInput);
        console.log(test);
        const input = document.getElementById('message-input') as any;
        input.value = '';

    }
    function handleMessage(value: string) {
        messageInput = value;
        // setMessages((messages) => messages = value);
    }

    return (
        <div className="expandable-input mt-4 relative">
            <textarea
                id="message-input"
                className="w-full p-2 bg-slate-800 rounded pr-20"
                placeholder="Type your message..."
                onChange={(event) => handleMessage(event.target.value)}
            ></textarea>
            <div className="token-count absolute right-2 bottom-2 text-gray-400 text-xs">
                0 tokens
            </div>
            <button
                id="send-message-btn"
                className={`px-4 py-2 rounded w-full mt-2`}
                onClick={sendMessage}
            >
                {isAgentActive ? 'Agent is busy...' : 'Send'}
            </button>
        </div >
        // <div></div>
    );
}