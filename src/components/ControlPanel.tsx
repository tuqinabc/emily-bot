"use client"

import { useMessageStore, useProjectStore } from "@/app/store";
import { create_project, get_messages, get_project_list } from "@/lib/lib";


export default function ControlPanel() {
    let selectedProject = '';
    const selectedModel = '';
    const tokenUsage = 0;
    const projectList = useProjectStore((state: any) => state.projects);
    const addProject = useProjectStore((state: any) => state.addProject);
    const addMessage = useMessageStore((state: any) => state.addMessage);


    // useEffect(() => {
    //     const fetchData = async () => {
    //         await create_project('');
    //     }
    //     fetchData();
    // }, [selectedProject]);

    async function handleProjects() {
        const projectDropdown = document.getElementById('project-dropdown');
        const data = await get_project_list();
        console.log('--->>>>', data);
        data.map((_: string) => addProject(_));
        projectDropdown?.classList.toggle('hidden');
    }

    function handleModels() {
        const modelDropdown = document.getElementById('model-dropdown');
        modelDropdown?.classList.toggle('hidden');
    }

    async function selectProject(project: string) {
        console.log(project)
        selectedProject = project;
        localStorage.setItem('selectedProject', project);
        // await create_project(project);
        const msgs = await get_messages(project);
        console.log('<--->', msgs);
        JSON.parse(msgs)?.map((m: string) => addMessage(m))
        handleProjects()
    }

    async function createNewProject() {
        const projectName = prompt('Enter the project name');

        if (projectName) {
            console.log('-->', projectName);
            const data = await create_project(projectName);
            addProject(projectName)
        }
    }

    // const internetStatus = document.getElementById('internet-status');
    // internetStatus?.classList.add('online');


    return (
        <div className="control-panel bg-slate-900 border border-indigo-700 rounded">
            <div className="dropdown-menu relative inline-block">
                <button
                    type="button"
                    className="inline-flex justify-center w-full gap-x-1.5 rounded-md bg-slate-900 px-3 py-2 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-indigo-700 hover:bg-slate-800"
                    id="project-button"
                    aria-expanded="true"
                    aria-haspopup="true"
                    onClick={handleProjects}
                >
                    <span id="selected-project">selected project</span>
                    <svg className="-mr-1 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill="evenodd" clip="evenodd"
                            d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                        />
                    </svg>
                </button>
                <div
                    id="project-dropdown"
                    className="absolute left-0 z-10 mt-2 w-full origin-top-left rounded-md bg-slate-800 shadow-lg ring-1 ring-indigo-700 ring-opacity-5 focus:outline-none hidden"
                    role="menu"
                    aria-orientation="vertical"
                    aria-labelledby="project-button"
                >
                    <div className="py-1" role="none">
                        <button className="text-white block px-4 py-2 text-sm hover:bg-slate-700" onClick={createNewProject}>
                            + Create new project
                        </button>

                        {projectList.map((_: string) =>
                            <button key={_} className="text-white block px-4 py-2 text-sm hover:bg-slate-700" onClick={() => selectProject(_)}>
                                {_}
                            </button>)}
                    </div>
                </div>
            </div>

            <div className="right-controls">
                <div className="flex items-center space-x-2">
                    <span>Internet:</span>
                    <div id="internet-status" className="internet-status online" />
                    <span id="internet-status-text" />
                </div>
                <div className="flex items-center space-x-2">
                    <span>Token Usage:</span>
                    <span id="token-count" className="token-count-animation">{tokenUsage}</span>
                </div>
                <div className="relative inline-block text-left">
                    <div>
                        <button
                            type="button"
                            className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-slate-900 px-3 py-2 text-sm font-semibold text-white shadow-sm ring-1 ring-inset ring-indigo-700 hover:bg-slate-800"
                            id="model-button"
                            aria-expanded="true"
                            aria-haspopup="true"
                            onClick={handleModels}
                        >
                            <span id="selected-model">selected Model</span>
                            <svg className="-mr-1 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path
                                    fill="evenodd"
                                    d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                                    clip="evenodd"
                                />
                            </svg>
                        </button>
                    </div>

                    <div
                        id="model-dropdown"
                        className="absolute right-0 z-10 mt-2 w-full origin-top-right rounded-md bg-slate-800 shadow-lg ring-1 ring-indigo-700 ring-opacity-5 focus:outline-none hidden"
                        role="menu"
                        aria-orientation="vertical"
                        aria-labelledby="model-button"
                    >
                        <div className="py-1" role="none">
                            <button
                                className="text-white block px-4 py-2 text-sm hover:bg-slate-700"
                            >
                                model1
                            </button>
                            <button
                                className="text-white block px-4 py-2 text-sm hover:bg-slate-700"
                            >
                                model2
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}
