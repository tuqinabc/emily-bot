export default function TerminalWidget() {
    return (
        <div className="flex flex-col bg-slate-950 border border-indigo-700 rounded flex-1 overflow-hidden">
            <div className="p-2 flex items-center border-b border-gray-700">
                <div className="flex space-x-2 ml-2 mr-4">
                    <div className="w-3 h-3 bg-red-500 rounded-full"></div>
                    <div className="w-3 h-3 bg-yellow-400 rounded-full"></div>
                    <div className="w-3 h-3 bg-green-500 rounded-full"></div>
                </div>
                <span id="terminal-title">Terminal</span>
            </div>
            <div
                id="terminal-content"
                className="bg-black flex-grow flex overflow-hidden"
            ></div>
        </div>
    );
}