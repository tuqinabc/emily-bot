import { dbConnection } from '@/lib/db';
import { planner_execute } from '@/lib/planner/planner';
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
    req: NextApiRequest, res: NextApiResponse
) {
    if (req.method === 'POST') {
        console.log('----->project, post');
        const body = JSON.parse(req.body);
        console.log(body);
        const ms = dbConnection("emily").collection("message");
        if (req.query?.method === 'add') {
            const exitingProject = await ms.findOne({ project: body.project });
            const msg = JSON.stringify([...JSON.parse(exitingProject?.message), body.message]);
            console.log(exitingProject);
            const d = await ms.findOneAndUpdate({ project: body.project }, { $set: { message: msg } });
            return res.status(200).json(d);
        }
        await ms.insertOne(body);
        return res.status(200).json(body);
    }
    if (req.method === 'GET') {
        try {
            const ms = dbConnection("emily").collection("message");

            if (req.query?.project === 'all') {
                console.log('----->project, getall');
                console.log('-->', req.query?.project);
                const data = await ms.find({}, { projection: { project: true, _id: false } }).toArray();
                return res.status(200).json(data);
            } else if (!!req.query?.project) {
                console.log('----->project, ', req.query?.project);
                const data = await ms.findOne({ project: req.query?.project }, { projection: { project: true, message: true, _id: false } });
                return res.status(200).json(data);
            }

        } catch (err) {
            console.log(err);
        }

    }
}