import { internal_monologue_execute } from '@/lib/internal_monologue/internal_monologue';
import { planner_execute } from '@/lib/planner/planner';
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
    req: NextApiRequest, res: NextApiResponse
) {
    console.log('---->llm')
    if (req.method === 'POST') {
        const body = JSON.parse(req.body);
        if (body.agent === 'plan') {
            const data = await planner_execute(body.prompt);
            return res.status(200).json({ data: data });
        }
        if (body.agent === 'internal') {
            const data = await internal_monologue_execute(body.prompt);
            return res.status(200).json({ data: data });
        }

    } else {
        const data = await planner_execute("help to write a function to calculate two numbers in javascript");
        return res.status(200).json({ data: data });
    }

}