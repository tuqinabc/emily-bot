import { dbConnection } from '@/lib/db';
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
    req: NextApiRequest, res: NextApiResponse
) {
    console.log('---->message')

    if (req.method === 'POST') {
        const body = req.body;
        const ms = dbConnection("emily").collection("message");
        await ms.insertOne(JSON.parse(body));
        return res.status(200).json(body);
    }
    if (req.method === 'GET') {
        const ms = dbConnection("emily").collection("message");
        console.log(req.query);
        const data = await ms.findOne({ project: '' });
        return res.status(200).json(data);
    }
}