import { MongoClient } from 'mongodb';

export function dbConnection(db: string) {
    const client = new MongoClient('mongodb://localhost:27017');
    const result = client.db(db);
    return result;
}

