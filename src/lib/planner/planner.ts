import mustache from "mustache";
import { readFileSync } from "fs";
import { chat } from "../chat";

export function planner_template_render(params: { prompt: string }) {
    const template = readFileSync('./src/lib/planner/planner.prompt.mustache', 'utf-8');
    return mustache.render(template, params);
}

export async function planner_execute(input: string) {
    const prompt = planner_template_render({ prompt: input });
    const response = await chat(prompt);
    return response;
}