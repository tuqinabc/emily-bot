import json5 from "json5";
export async function create_project(project: string) {
    const data = await fetch('http://localhost:3333/api/project', { method: 'POST', body: JSON.stringify({ project: project, message: '[]' }) });
    const result = await data.json();
    return result;
}

export async function get_project_list() {
    const data = await fetch('http://localhost:3333/api/project?project=all');
    const response = await data.json();
    return response.map((_: any) => _.project);
}
export async function add_message_to_project(project: string, message: string) {
    const data = await fetch('http://localhost:3333/api/project?method=add', { method: 'POST', body: JSON.stringify({ project: project, message: message }) });
    const result = await data.json();
    return result;
}

export async function add_message_from_user(project: string, message: string) {
    const data = await add_message_to_project(project, JSON.stringify(new_message(false, message)));
    return data;
}

export async function add_message_from_emily(project: string, message: string) {
    const data = await add_message_to_project(project, JSON.stringify(new_message(true, message)));
    return data;
}

export async function get_messages(project: string) {
    const data: any = await fetch(`http://localhost:3333/api/project?project=${project}`);
    const messages = (await data.json()).message;
    return messages;
}

export async function get_latest_message_from_user(project: string) {
    const message = await get_messages(project);
    return '';
}
export async function get_latest_message_from_emily(project: string) {
    const message = await get_messages(project);
    return '';
}
export async function get_all_messages_formated(project: string) {
    return [];
}

export function new_message(from_emily: boolean, message: string) {
    return {
        'from_emily': from_emily,
        'message': message,
        'timestamp': Date.now().toLocaleString()
    }
}
export async function execute_agent(project: string, message: string) {
    console.log('---send messages----')
    const data = await fetch('http://localhost:3333/api/llm', { method: 'POST', body: JSON.stringify({ agent: 'plan', prompt: message }) });
    const result = await data.json();
    const planed_result = parse_planner_response(result.data)
    console.log(`planed_result-> ${planed_result}`);
    // add_message_from_emily(project, planed_result['reply']);
    // add_message_from_emily(project, json5.stringify(planed_result['plans']));

    const internal_data = await fetch('http://localhost:3333/api/llm', { method: 'POST', body: JSON.stringify({ agent: 'internal', prompt: result.data }) });
    const internal_data_result = await internal_data.json();

    console.log(`internal_data_result-> ${internal_data_result}`);

    return result;
}

export function parse_planner_response(response: string) {
    const result = {
        "project": "",
        "reply": "",
        "focus": "",
        "plans": {} as any,
        "summary": ""
    };

    let current_section = '';
    let current_step = '';
    response.split('\n').map(line => {
        if (line.startsWith('Project Name')) {
            current_section = 'project';
            result['project'] = line.split(':', 2)[1];
        } else if (line.startsWith('Your Reply to the Human Prompter:')) {
            current_section = 'reply';
            result['reply'] = line.split(':', 2)[1];
        } else if (line.startsWith('Current Focus:')) {
            current_section = 'focus';
            result['focus'] = line.split(':', 2)[1];
        } else if (line.startsWith('Plan:')) {
            current_section = 'plans';
        } else if (line.startsWith('Summary:')) {
            current_section = 'summary';
            result['summary'] = line.split(':', 2)[1];
        } else if (current_section === 'reply') {
            result['reply'] += ' ' + line;
        } else if (current_section === 'focus') {
            result['focus'] += ' ' + line;
        } else if (current_section === 'plans') {
            if (line.startsWith('- [ ] Step')) {
                current_step = line.split(':')[0].split(' ').slice(-1)[0];
                console.log(current_step, result['plans']);
                result['plans'][Number(current_step)] = line.split(':', 2)[1];
            } else if (!!current_step) {
                result['plans'][Number(current_step)] += " " + line;
            }
        } else if (current_section === 'summary') {
            result['summary'] += ' ' + line.replace('```', '').replace('<end>', '');
        }
    })
    return result;
}


