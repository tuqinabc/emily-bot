import mustache from "mustache";
import { readFileSync } from "fs";
import { chat } from "../chat";

export function internal_monologue_template_render(params: { current_prompt: string }) {
    const template = readFileSync('./src/lib/internal_monologue/internal_monologue.prompt.mustache', 'utf-8');
    return mustache.render(template, params);
}

// export function validate_response(response: string) {
//     let res = response.trim().replace('```json', '```');

//     if (res.startsWith('```') && res.endsWith('```')) {
//         res = res.substring(3, res.length - 3).trim()
//     }
//     return res['internal_monologue'];
// }

export async function internal_monologue_execute(current_prompt: string) {
    const prompt = internal_monologue_template_render({ current_prompt: current_prompt });
    const response = await chat(prompt);
    console.log(response);
    return response;
}