export function code_set_to_markdown(project_name: string) {
    const code_set = read_directory() as any;
    let markdown = '';

    code_set?.map((c: any) => {
        markdown += `### ${c['filename']}: \n\n`
        markdown += `\`\`\`\n${c['code']}: \n\n`
        markdown += '---\n\n'
    })
}
export function read_directory() {
    const files_list = [];
    return files_list;
}