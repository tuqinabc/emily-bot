import { LLM } from "llama-node";
import path from "path";
import { LLamaCpp } from "./llm/llama-cpp";



export async function chat(prompt: string) {
    const model = path.resolve(process.cwd(), "");
    const llama = new LLM(LLamaCpp);
    const config = {
        modelPath: model,
        enableLogging: true,
        nCtx: 4096,
        seed: 0,
        f16Kv: false,
        logitsAll: false,
        vocabOnly: false,
        useMlock: false,
        embedding: false,
        useMmap: true,
        nGpuLayers: 0
    };
    await llama.load(config);
    const params = {
        nThreads: 4,
        nTokPredict: 9000,
        topK: 40,
        topP: 0.1,
        temp: 0.2,
        repeatPenalty: 1,
        prompt: `${prompt}`
    };
    console.log(params);
    let result = '';
    await llama.createCompletion(params, (response) => {
        result += response.token;
        process.stdout.write(response.token);
    });
    return result;
}
