import Sidebar from "@/components/Sidebar";
import ControlPanel from "@/components/ControlPanel";
import MessageContainer from "@/components/MessageContainer";
import InternalMonologue from "@/components/InternalMonologue";
import MessageInput from "@/components/MessageInput";
import BrowserWidget from "@/components/BrowserWidget";
import TerminalWidget from "@/components/TerminalWidget";
import { get_project_list } from "@/lib/lib";


export default async function Home() {
  // localStorage.clear();
  // const intervalId = setInterval(async () => {


  // }, 1000);



  return (
    <main className="h-dvh ml-20">
      <Sidebar />
      <div className="flex flex-col p-4 h-full">
        <ControlPanel />
        <div className="flex h-full space-x-4">
          <div className="flex flex-col w-1/2">
            <MessageContainer />
            <InternalMonologue />
            <MessageInput />
          </div>
          <div className="flex flex-col w-1/2 space-y-4">
            <BrowserWidget />
            <TerminalWidget />
          </div>
        </div>
      </div>
    </main >
  );
}

