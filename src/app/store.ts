import { get_project_list } from '@/lib/lib';
import { create } from 'zustand';
export const useMessageStore = create((set) => ({
    messages: [],
    addMessage: (message: string) => set((state: any) => ({ messages: state.messages.filter((msg: string) => msg === message).length > 0 ? state.message : [...state.messages, message] })),
}));

export const useProjectStore = create((set) => ({
    projects: [],
    addProject: (project: string) => set((state: any) => ({ projects: state.projects.filter((proj: string) => proj === project).length > 0 ? state.projects : [...state.projects, project] })),
}));
